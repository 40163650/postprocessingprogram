#pragma once
#include "Matrix4.h"

void drawTriangle(Vector3 pointA, Vector3 pointB, Vector3 pointC);

void drawQuad(Vector3 pointA, Vector3 pointB);

void drawSquare(Vector3 pointA, float sideLength, int axis = 0);

void drawCircle(Vector3 centre, float radius);