#pragma once

// GLEW (GL extension wrangler)
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// IOSTREAM
#include <iostream>

// FSTREAM
#include <fstream>

// STRING
#include <string>

// VECTOR
#include <vector>

// SOIL (simple opengl image library)
#include "SOIL.h"

// Joey De Vries's shader class
#include "Shader.h"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);