#include "Header.h"
using namespace std;

/*
	This program was developed using GLFW and GLEW. They will need to be
	present as libraries on the user's machine where this executable is.
	This program is based heavily on the tutorials from the following site:
	https://learnopengl.com/
	It also uses SOIL and some standard libraries, see "Header.h" for more
*/

/*
	TODO:
		Some kind of V-Sync or frame rate cap to reduce noise
*/

#define FILE_NAME_LENGTH 127

// Window dimensions
const GLuint WIDTH = 512, HEIGHT = 512;

// The window of the program. Be careful with memory, pointers and references
GLFWwindow* window;

// Config file information
string shaderFileName;
vector<string> imageNames;

// The safe way of making sure the uniforms are set correctly
const GLchar *uniformNames[] = {
	"ourTexture1",
	"ourTexture2",
	"ourTexture3",
	"ourTexture4", 
	"ourTexture5", 
	"ourTexture6", 
	"ourTexture7", 
	"ourTexture8", 
	"ourTexture9", 
	"ourTexture10", 
	"ourTexture11", 
	"ourTexture12", 
	"ourTexture13", 
	"ourTexture14", 
	"ourTexture15", 
	"ourTexture16", 
	"ourTexture17", 
	"ourTexture18", 
	"ourTexture19", 
	"ourTexture20", 
	"ourTexture21", 
	"ourTexture22", 
	"ourTexture23", 
	"ourTexture24", 
	"ourTexture25", 
	"ourTexture26", 
	"ourTexture27", 
	"ourTexture28", 
	"ourTexture29", 
	"ourTexture30", 
	"ourTexture31", 
	"ourTexture32", 
};

// Variables for setting window size
int maxImageHeight = 0, maxImageWidth = 0;
int screenHeight, screenWidth;
int windowWidth, windowHeight;

// Shader variables
GLuint shaderProgram;
GLuint VBO, VAO, EBO;
Shader ourShader;

// Texture variables
vector<GLuint> textures;

// Framerate information
double lastTime = glfwGetTime();
int nbFrames = 0;
float timeElapsed = 0;

// Wireframe mode
bool wireFrame = false;

// Creates a substring.
string findStringBetweenChar(string haystack, string needle)
{
	size_t needle1Pos = haystack.find_first_of(needle);
	size_t needle2Pos = haystack.find_last_of(needle);
	return haystack.substr(needle1Pos + 1, needle2Pos - needle1Pos - 1); // Numbers are here to strip the quotes
}

// Reads the config file and stores the data in global variables
void readConfigFile()
{
	char data[FILE_NAME_LENGTH];
	const char * configFilePath = "Config.cfg";
	FILE * configFile;

	vector<string> configLines = {};

	configFile = fopen(configFilePath, "r");

	if (configFile != NULL)
	{
		//For now, let's assume the user wrote the config file correctly
		cout << "Reading config file..." << endl;
		while (!feof(configFile))
		{
			fgets(data, FILE_NAME_LENGTH, configFile);
			configLines.push_back(data);
		}
		fclose(configFile);

		shaderFileName = findStringBetweenChar(configLines[0], "\"");

		cout << "SHADER FILE NAME: " << shaderFileName << endl;

		// i is 2 because we ignore the first 2 lines to get to the image paths
		for (unsigned int i = 2; i < configLines.size(); i++)
		{
			imageNames.push_back(findStringBetweenChar(configLines[i], "\""));
			cout << "IMAGE " << i - 2 << ": " << imageNames[i - 2] << endl;
		}

		if (imageNames.size() == 0)
		{
			cout << "NO IMAGE USED" << endl;
		}
		else if (imageNames.size() == 1)
		{
			cout << "ONLY USED ONE IMAGE" << endl;
		}
		else
		{
			cout << "USED " << imageNames.size() << " IMAGES" << endl;
		}
	}
	else
	{
		cout << "[ERROR]: Couldn't find the config file or the path was more than 128 bytes." << endl;
	}

	// Final check just in case
	if (shaderFileName == "")
	{
		cout << "[ERROR]: SHADER FILE NAME IS EMPTY, please check the config file." << endl;
	}
}

// Sets up the shaders
void loadShaders()
{
	const GLchar* SFN = shaderFileName.c_str();
	ourShader = Shader("shaders/joey.vert", SFN);
}

// Sets up a quad to draw to the screen
void setUpQuad()
{
	// Set up vertex data (and buffer(s)) and attribute pointers

	/*
	  Texture Coords
	  01 11
	  00 10

	  Positions
	  (-1,+1) (+1,+1)
	  (-1,-1) (+1,-1)

	  Colours
	  110 100
	  001 010
	*/

	GLfloat vertices[] = {
		// Positions          // Colors           // Texture Coords
		 1.0f,  1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // Top Right
		 1.0f, -1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Right
		-1.0f, -1.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // Bottom Left
		-1.0f,  1.0f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // Top Left 
	};

	GLuint indices[] = {
		0, 1, 3, // First Triangle
		1, 2, 3  // Second Triangle
	};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	// Color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	// TexCoord attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	glBindVertexArray(0); // Unbind the VAO
}

// Sets up the textures for rendering
void loadTextures()
{
	textures = vector<GLuint>(imageNames.size());
	for (unsigned int i = 0; i < imageNames.size(); i++)
	{
		glGenTextures(1, &textures[i]);
		glBindTexture(GL_TEXTURE_2D, textures[i]); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object
		// Set our texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// Set texture filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Load, create texture and generate mipmaps
		int width, height;
		unsigned char* image = SOIL_load_image(imageNames[i].c_str(), &width, &height, 0, SOIL_LOAD_RGB);
		// Set the maximum width and height values to resize the window
		if (width > maxImageWidth)
			maxImageWidth = width;
		if (height > maxImageHeight)
			maxImageHeight = height;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(image);
		glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.
	}
}

// Sets the screen size variables
void getScreenSize()
{
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	screenWidth = mode->width;
	screenHeight = mode->height;
}

// Draws a quad to the screen (called every frame)
void drawQuad()
{
	glBindVertexArray(VAO);

	if (wireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

// Prints the current framerate
void printFrameRate()
{
	double currentTime = glfwGetTime();
	nbFrames++;
	if (currentTime - lastTime >= 1.0)
	{ // If last prinf() was more than 1 sec ago printf and reset timer
		printf("%d FPS\n", nbFrames);
		nbFrames = 0;
		lastTime += 1.0;
	}
}

// Gets the keys pressed and creates events for them. Prints them out. Closes on ESC
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//cout << "The " << key << " was pressed" << endl;

	// When a user presses the escape key, we set the WindowShouldClose property to true, 
	// closing the application
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		loadShaders();

	if (key == GLFW_KEY_W && action == GLFW_PRESS)
		wireFrame = true;

	if (key == GLFW_KEY_W && action == GLFW_RELEASE)
		wireFrame = false;
}

// Gets the state of the window when it changes
void window_focus_callback(GLFWwindow* window, int focused)
{
	if (focused)
	{
		// The window gained input focus
		loadShaders();
	}
	else
	{
		// The window lost input focus
	}
}

// When the user resizes the window, adjust the image to fit it
void window_size_callback(GLFWwindow* window, int width, int height)
{
	glfwGetWindowSize(window, &windowWidth, &windowHeight);
	glViewport(0, 0, windowWidth, windowHeight);
}

// Set up the GLEW (to experimental)
void initialiseGLEW()
{
	try
	{
		glewExperimental = GL_TRUE;
		glewInit();
	}
	catch (int exception)
	{
		cout << "Failed to initialize GLEW. Exception no: " << exception << endl;
	}
}

// Set up the program
void initialise()
{
	// Assign values to the shader file name and images etc.
	readConfigFile();
	// Set up GLFW
	cout << "Initialising" << endl;
	glfwInit();
	cout << "Setting OpenGL verision to 3.3" << endl;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create Window
	window = glfwCreateWindow(WIDTH, HEIGHT, "Post Processing Program", nullptr, nullptr);
	if (window == nullptr)
	{
		cout << "Failed to create GLFW window" << endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);
	cout << "Window successfully created" << endl;

	// Register the key callback with OpenGL
	glfwSetKeyCallback(window, key_callback);
	cout << "Key callback successfully registered\n"
		<< "Press \"ESC\" to quit\n"
		<< "Press \"R\" to reload the shader\n"
		<< "Hold \"W\" to view wireframe" << endl;

	// Register the window-focus callback
	glfwSetWindowFocusCallback(window, window_focus_callback);

	// Register the window-resize callback
	glfwSetWindowSizeCallback(window, window_size_callback);

	// Set GLEW to experimental mode
	initialiseGLEW();
	cout << "GLEW successfully initialised" << endl;

	// Set up the viewport, define its dimensions
	//int width, height;
	//glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, WIDTH, HEIGHT);
	cout << "Viewport successfully set up" << endl;

	loadShaders();
}

// Do some things at the start that you only want to do once
void startUp()
{
	loadTextures();
	getScreenSize();

	// Resize window to the size of the largest image or the screen if it's too big
	// Then set the aspect ratio to the largest image
	// Then redo the viewport so the image fills it
	if(maxImageWidth > screenWidth || maxImageHeight > screenHeight)
	{
		glfwSetWindowSize(window, screenWidth, screenHeight);
		glfwSetWindowAspectRatio(window, maxImageWidth, maxImageHeight);
		glViewport(0, 0, screenWidth, screenHeight);
	}
	else
	{
		glfwSetWindowSize(window, maxImageWidth, maxImageHeight);
		glfwSetWindowAspectRatio(window, maxImageWidth, maxImageHeight);
		glViewport(0, 0, maxImageWidth, maxImageHeight);
	}
	
	setUpQuad();
}

// Run the program. Called every frame until glfwSetWindowShouldClose is GL_FALSE, includes rendering
void tick()
{
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		ourShader.Use();

		for (unsigned int i = 0; i < textures.size() && i < 32; i++)
		{
			GLenum target = GL_TEXTURE_2D;

			// The equivalent of glBindTextures in GLSL 4.4 and up.
			// See: https://www.opengl.org/sdk/docs/man/html/glBindTextures.xhtml
			// Or for the full documentation: https://www.opengl.org/sdk/docs/man
			GLuint texture;
			if (textures.size() == NULL)
			{
				texture = 0;
			}
			else
			{
				texture = textures[i]; // Without && i < 32 it will crash here.
			}
			glActiveTexture(GL_TEXTURE0 + i); // First is 0 so need not add it.
			if (texture != 0)
			{
				glBindTexture(target, texture);
			}
			else
			{
				glBindTexture(target, 0);
			}
			glUniform1i(glGetUniformLocation(ourShader.Program, uniformNames[i]), i);
		}
		glUniform1i(glGetUniformLocation(ourShader.Program, "WINDOW_WIDTH"), windowWidth);
		glUniform1i(glGetUniformLocation(ourShader.Program, "WINDOW_HEIGHT"), windowHeight);

		double currentTime = glfwGetTime();
		float smoothness = 0.001f;
		if (currentTime - timeElapsed >= smoothness)
		{
			timeElapsed += smoothness;
		}
		glUniform1f(glGetUniformLocation(ourShader.Program, "TIME_ELAPSED"), timeElapsed);

		drawQuad();

		//printFrameRate();

		glfwSwapBuffers(window);
	}
}

// Halt the program.
void terminateProgram()
{
	cout << "Stopping" << endl;
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
	glfwTerminate();
}

int main()
{
	// Set up the program
	initialise();

	// Do certain things only once
	startUp();

	// The main loop of the program, things that are done every frame
	cout << "Running!" << endl;
	tick();

	// Close the program
	terminateProgram();
	return 0;
}