#version 330 core

in vec3 ourColor;
in vec2 TexCoord;

out vec4 color;

uniform int WINDOW_WIDTH;
uniform int WINDOW_HEIGHT;
uniform float TIME_ELAPSED; // Number of seconds elapsed since the start of the program. To 3 decimal places.

float INV_WIDTH = (1.0 / WINDOW_WIDTH);
float INV_HEIGHT = (1.0 / WINDOW_HEIGHT);

// Texture sampler array
//uniform sampler2D ourTextureArray[32]; // This is restricted by OpenGL

// DO NOT EDIT ABOVE THIS LINE

// To use a specific texture you need to say "uniform sampler2D ourTextureX;" where X is the position you
// set the texture as in the config file. "ourTexture1" is the first texture

uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;
uniform sampler2D ourTexture3;

// YOU MAY NOT USE MORE THAN 32 TEXTURES

// NB: IF THE TEXTURE IS WHITE OR BLACK THERE MAY BE A TYPO IN YOUR CODE, it should tell you in the command line

vec4 sobel()
{
	vec4 top = texture(ourTexture1, vec2(TexCoord.x, TexCoord.y + INV_HEIGHT));
	vec4 bottom = texture(ourTexture1, vec2(TexCoord.x, TexCoord.y - INV_HEIGHT));

	vec4 left = texture(ourTexture1, vec2(TexCoord.x - INV_WIDTH, TexCoord.y));
	vec4 right = texture(ourTexture1, vec2(TexCoord.x + INV_WIDTH, TexCoord.y));

	vec4 tl = texture(ourTexture1, vec2(TexCoord.x - INV_WIDTH, TexCoord.y + INV_HEIGHT));
	vec4 tr = texture(ourTexture1, vec2(TexCoord.x + INV_WIDTH, TexCoord.y + INV_HEIGHT));
	vec4 bl = texture(ourTexture1, vec2(TexCoord.x - INV_WIDTH, TexCoord.y - INV_HEIGHT));
	vec4 br = texture(ourTexture1, vec2(TexCoord.x + INV_WIDTH, TexCoord.y - INV_HEIGHT));

	//right = right * vec4(1.0, 0.0, 1.0, 1.0);
	//left = left * vec4(0.0, 1.0, 1.0, 1.0); // Not in original sobel

	vec4 sx = -tl - 2 * left - bl + tr + 2 * right + br;
	vec4 sy = -tl - 2 * top - tr + bl + 2 * bottom + br;

	vec4 returnCol;// = sqrt(sx * sx + sy * sy);
	returnCol = sx * sx + sy * sy;

	returnCol.w = 1.0;

	return returnCol;
}

void main()
{
	// Make sure you return a vec4 and the alpha (.w) is 1.0

	// Here is an example of something you can do with 3 textures
	//color = vec4(texture(ourTexture1, TexCoord).r,
	//			 texture(ourTexture2, TexCoord).g,
	//			 texture(ourTexture3, TexCoord).b,
	//			 1.0);
	//color = sobel();

	vec4 original = texture(ourTexture1, TexCoord);
	color = vec4(abs(sin(TIME_ELAPSED)), original.g, original.b, 1.0);

	//vec4 out_color = texture(ourTexture2, TexCoord) * (1.0 / sobel());
	//color = vec4(pow(out_color.r, 1.5), pow(out_color.g, 1.5), pow(out_color.b, 1.5), 1.0);

	//color = texture(ourTexture1, TexCoord);
}