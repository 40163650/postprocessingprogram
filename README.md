# README #

### What is this repository for? ###

This is the repository for a program I intend to distribute to people learning OpenGL Shader Language for them to try out shaders on image files.

### How do I get set up? ###

* Edit the config file to set up which image and shader you intend to use.
* Set up your images and shaders.
* Run the program.
* The shader file is automatically reloaded when the program window is re-selected. If you change the config file you will have to close the program for your changes to take effect.

### Contribution guidelines ###

This is a solo project.

### Who do I talk to? ###

If you have trouble getting set up email sirawesomnessmodtoggler@gmail.com